/*
MariaDB Backup
Database: ownk
Backup Time: 2022-05-27 13:50:39
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `ownk`.`users`;
DROP FUNCTION IF EXISTS `ownk`.`delete_user`;
DROP FUNCTION IF EXISTS `ownk`.`get_user`;
DROP FUNCTION IF EXISTS `ownk`.`get_users`;
DROP FUNCTION IF EXISTS `ownk`.`post_user`;
DROP FUNCTION IF EXISTS `ownk`.`put_user`;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE DEFINER=`dev`@`%` PROCEDURE `delete_user`(
    IN _id char(36)
                                           )
BEGIN
    DELETE
    FROM users
    WHERE id = _id;
END;
CREATE DEFINER=`dev`@`%` PROCEDURE `get_user`(
IN _id         CHAR(36)
)
BEGIN
		SELECT
	users.id, 
	users.name,
	users.age
FROM users
WHERE
	users.id = _id;
END;
CREATE DEFINER=`dev`@`%` PROCEDURE `get_users`()
BEGIN
    SELECT
        users.id,
        users.name,
        users.age
    FROM users;
END;
CREATE DEFINER=`dev`@`%` PROCEDURE `post_user`(
		IN _id CHAR(36),
    IN _name VARCHAR(255),
    IN _age INT
)
BEGIN
  INSERT INTO users(
	users.id,
	users.name,
	users.age
  ) VALUES(
	_id,
	_name,
	_age
  );
END;
CREATE DEFINER=`dev`@`%` PROCEDURE `put_user`(
		IN _id CHAR(36),
    IN _name VARCHAR(255),
    IN _age INT
		)
BEGIN
  UPDATE users SET
		users.name = _name,
		users.age = _age
  WHERE 
		users.id = _id;
END;
BEGIN;
LOCK TABLES `ownk`.`users` WRITE;
DELETE FROM `ownk`.`users`;
INSERT INTO `ownk`.`users` (`id`,`name`,`age`) VALUES ('6cd12fc9-ac31-4e4e-bb9a-43683caaabc2', 'aaaa', 23),('b23b183c-ddcb-11ec-a757-f85ea003ddf6', 'test', 6),('ca5cd567-a9c6-44b3-8f0d-ef7896132fae', 'peaa', 28);
UNLOCK TABLES;
COMMIT;
