package com.testpost.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;


public class HttpAPIResponse {
    private HttpStatus httpStatus;
    private int httpStatusCode;
    private String message;
    private HttpHeaders headers;
    private Object body;
    private int registers;
    private int pages;
    private Map<String, Object> response = new HashMap<String, Object>();


    public HttpAPIResponse(HttpHeaders headers, Object body, String message) {
        this.headers = headers;
        this.body = body;
        this.message = message;
    }

    public HttpAPIResponse(Object body, int registers, int pages) {
        this.body = body;
        this.registers = registers;
        this.pages = pages;
    }

    public HttpAPIResponse(Object body, String message) {
        this.body = body;
        this.message = message;
    }

    public HttpAPIResponse(Object body) {
        this.body = body;
    }

    public HttpAPIResponse(String message) {
        this.message = message;
    }
    // 200 COMPLETO
    // success
    public ResponseEntity OK(){
        this.setStatus(HttpStatus.OK);
        this.build();
        return new ResponseEntity<Object>(this.response, this.headers, this.httpStatus);
    }

    //401 SIN AUTORIZACIÓN
    public ResponseEntity UNAUTH(){
        this.setStatus(HttpStatus.UNAUTHORIZED);
        this.build();
        return new ResponseEntity<Object>(this.response, this.headers, this.httpStatus);
    }

    //403 PROHIBIDO
    public ResponseEntity FORBIDDEN(){
        this.setStatus(HttpStatus.FORBIDDEN);
        this.build();
        return new ResponseEntity<Object>(this.response, this.headers, this.httpStatus);
    }

    //500 ERROR DEL SERVIDOR
    public ResponseEntity ERROR(){
        this.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        this.build();
        return new ResponseEntity<Object>(this.response, this.headers, this.httpStatus);
    }

    public void setStatus(HttpStatus status) {
        this.httpStatus = status;
        this.httpStatusCode = this.httpStatus.value();
    }

    public void build() {
        this.response.put("statusCode", this.httpStatusCode);
        this.response.put("status", this.httpStatus);
        if(this.message != null) this.response.put("message", this.message);
        if(this.registers != 0) this.response.put("registers", this.registers);
        if(this.pages != 0) this.response.put("pages", this.pages);
        this.response.put("body", this.body);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
