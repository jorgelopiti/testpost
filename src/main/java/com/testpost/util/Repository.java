package com.testpost.util;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Repository {
    private String UDB;
    private String DB;
    private Connection connection;
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private String URL = SystemFingerprint.SQLDRIVER;
    private CallableStatement cs = null;
    private ResultSet rs = null;
    private String query;
    private Map params = new HashMap<String, Object>();
    private String _params = "";

    public Repository(){
        this.UDB = SystemFingerprint.UDB;
        this.DB = SystemFingerprint.APPDB;
    }

    protected void call(String query) {
        this.query = query;
    }

    protected void setParam(String key, Object value){
        this._params += " ?,";
        this.params.put(key, value);
    }

    protected void setParamOut(String key, Object value){
        this._params += " ?,";
        this.params.put("O-" + key, value);
    }

    protected Object getParam(String key, Class type) throws SQLException {
        if(type == Integer.class) {
            return cs.getInt(key);
        }
        return null;
    }

    protected ResultSet executeQuery() throws SQLException {
        if(!this.params.isEmpty()) this._params = this._params.substring(0, this._params.length() - 1);
        this.query = "{call " + this.query + "(" + this._params + " )}";
        System.out.println(query);
        cs = MysqlCoommand(this.query);


        if(!this.params.isEmpty()) {
            this.params.forEach((k,v)->{
                if(v != null) {
                    if(k.toString().contains("O-")) {
                        var a = 0;
                        if (Integer.class.equals(v)) {
                            try {
                                String key = k.toString().replace("O-", "");
                                cs.registerOutParameter(key, Types.INTEGER);
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        }

                    } else {
                        if (Integer.class.equals(v.getClass())) {
                            try {
                                cs.setInt(k.toString(), Integer.parseInt(v.toString()));
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        } else if (Boolean.class.equals(v.getClass())) {

                            try {
                                cs.setBoolean(k.toString(), Boolean.parseBoolean(v.toString()));
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }

                        } else if (String.class.equals(v.getClass()) || v.getClass() == java.util.UUID.class ) {

                            try {
                                cs.setString(k.toString(), v.toString());
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        }
                    }
                } else {
                    try {
                        cs.setString(k.toString(), null);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            });
        }


        rs = cs.executeQuery();
        return rs;
    }


    protected CallableStatement MysqlCoommand(String command) throws SQLException {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
//            URL = URL + companyRoom + "?autoReconnect=true&useSSL=false";
        URL = URL + DB + "?useSSL=false";
        connection = DriverManager.getConnection(URL, UDB, SystemFingerprint.UDBPASS);
        return connection.prepareCall(command);
    }

    protected void closeConnection() throws SQLException {
        this.closeConnection(cs, rs);
    }

    protected void closeConnection(PreparedStatement statement, ResultSet resultSet) throws SQLException{
        closeConnection(connection, statement);

        if (resultSet != null) {
            resultSet.close();
        }
//        Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, e);
    }

    public static void closeConnection(Connection connection, PreparedStatement statement) throws SQLException{
        closeConnection(connection);

        if (statement != null) {
            statement.close();
        }
    }

    protected void closeConnection(PreparedStatement statement) throws SQLException{
        closeConnection(connection, statement);
    }

    public static void closeConnection(Connection connection) throws SQLException{
        if (connection != null) {
            connection.close();
        }
    }

}
