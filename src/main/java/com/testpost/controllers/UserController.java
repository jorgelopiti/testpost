package com.testpost.controllers;

import com.testpost.AppApi;
import com.testpost.user.User;
import com.testpost.user.UserRepository;
import com.testpost.util.HttpAPIResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("v1/users")
public class UserController {
    private AppApi _appApi = new AppApi();

    @GetMapping
    @ResponseBody
    ResponseEntity get() {
        try{
            var users = _appApi.user().get();
            return new HttpAPIResponse(users).OK();
        }catch (Exception e) {
            return new HttpAPIResponse(e.getMessage()).ERROR();
        }
    }

    /*@RequestMapping(value = "/{id}", method = GET)
    @ResponseBody
    public ResponseEntity get(@PathVariable String id) {
        try {
            var user = _appCore.user().get(id);
            return new HttpAPIResponse(user).OK();
        }
        catch (Exception e){
            return new HttpAPIResponse(e.getMessage()).ERROR();
        }
    }*/

    @RequestMapping(method = POST)
    public ResponseEntity post(@RequestBody User user) {
        UserRepository bagRepository = new UserRepository();
        user.setId(UUID.randomUUID());
        try {
            bagRepository.post(user);
            return new HttpAPIResponse(user, "Usuario creado con éxito").OK();
        }catch (Exception e) {
            return new HttpAPIResponse(e.getMessage()).ERROR();
        }
    }

    @RequestMapping(method = PUT)
    public ResponseEntity put(@RequestBody User user){
        UserRepository bagRepository = new UserRepository();
        try {
           bagRepository.put(user);
            return new HttpAPIResponse(user, "usuario actualizado con exito").OK();
        }catch (Exception e) {
            return new HttpAPIResponse(e.getMessage()).ERROR();
        }
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity delete(@PathVariable UUID id) {
        try {
            _appApi.user().delete(id.toString());
            return new HttpAPIResponse("usuario eliminado").OK();
        }catch (Exception e) {
            return new HttpAPIResponse(e.getMessage()).ERROR();
        }
    }
}
