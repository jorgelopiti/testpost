package com.testpost.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserFactory {
    static User get(ResultSet resultSet) throws SQLException{
        var user = new User();
        try {
            if (resultSet != null) {

                user.setId(resultSet.getString("id") != null ? UUID.fromString(resultSet.getString("id")) : null );
                user.setName(resultSet.getString("name"));
                user.setAge(resultSet.getInt("age"));
            }
            return user;
        } catch (SQLException e) {
            throw e;
        }
    }
}