package com.testpost.user;

import com.testpost.util.Repository;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserRepository extends Repository {

    public void put(User user) throws SQLException{
        CallableStatement callableStatement = null;

        callableStatement = MysqlCoommand("{call put_user(?, ?, ?)}");
        callableStatement.setString("_id", user.getId() != null ? user.getId().toString() : null);
        callableStatement.setString("_name", user.getName());
        callableStatement.setInt("_age", user.getAge());
        callableStatement.executeQuery();
        this.closeConnection(callableStatement);
    }

    public ArrayList get() throws SQLException {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        var users = new ArrayList();

        callableStatement = MysqlCoommand("{call get_users()}");
        resultSet = callableStatement.executeQuery();
        while (resultSet.next()) {
            users.add(UserFactory.get(resultSet));
        }
        this.closeConnection(callableStatement, resultSet);
        return users;

    }

    public void post(User user) throws SQLException {
        CallableStatement callableStatement = null;

        callableStatement = MysqlCoommand("{call post_user(?, ?, ?)}");
        callableStatement.setString("_id", user.getId() != null ? user.getId().toString() : null);
        callableStatement.setString("_name", user.getName());
        callableStatement.setInt("_age", user.getAge());
        callableStatement.executeQuery();
        this.closeConnection(callableStatement);
    }

    public void delete(String id) throws SQLException {
        call("delete_user");
        setParam("_id", id);
        executeQuery();
        this.closeConnection();
    }
}
