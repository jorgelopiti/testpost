# testpost

## Database configuration

1. Run this mysql backup file (bk.sql) in your local database
   location: 'testpost/src/main/resources/database_rsc'.

    ### Note: 
    This file contains the database structure with the respective stored procedures.

2. Configure the database connection credentials in the class testpost/src/main/java/com/testpost/util/SystemFingerprint.java taking into account the variables:
- "UDB": database user.
- "UDBPASS": Database password
- "APPDB": Name of the database

## To run this project

1. Navigate to the project folder in the terminal/console.
2. Run in the root of the project: gradle :bootRun 

